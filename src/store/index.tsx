import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import throttle from 'lodash/throttle';

import { masterclassReducers } from "./masterclasses/reducers";
import {authReducers} from './auth/reducers';

const rootReducer = combineReducers({
  masterclasses: masterclassReducers,
  auth: authReducers,
});

export type AppState = ReturnType<typeof rootReducer>;

const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

const saveState = (state: any) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (err) {}
};

export default function configureStore() {
  const middlewares = [thunkMiddleware];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  const store = createStore(
    rootReducer,
    loadState(),
    composeWithDevTools(middleWareEnhancer)
  );

  store.subscribe(
    throttle(() => {
      saveState({
        auth: store.getState().auth,
        masterclasses: store.getState().masterclasses,
      });
    },
    1000));


  return store;
}
