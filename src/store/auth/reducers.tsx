import { combineReducers} from "redux";

import * as types from './types';

const isLoggingIn = (state = false, action: types.LoginActionTypes) => {
  switch (action.type) {
      case types.LOGIN_STARTED:
          return true;
      case types.LOGIN_SUCCESS:
      case types.LOGIN_FAILED:
          return false;
      default:
          return state;
  }
}

const isLoggedIn = (state = false, action: types.LoginActionTypes) => {
  switch (action.type) {
      case types.LOGIN_SUCCESS:
        return true;
      case types.LOGIN_FAILED:
      case types.LOGIN_STARTED:
        return false;
      default:
          return state;
  }
}

const token = (state = '', action: types.LoginActionTypes) => {
	switch(action.type) {
		case types.LOGIN_SUCCESS:
			return action.token
		default: 
			return state
	}
}

const user = (state = {}, action: types.LoginActionTypes) => {
	switch(action.type) {
		case types.LOGIN_SUCCESS:
			return action.user
		default:
			return state
	}
}

export const authReducers = combineReducers({
  isLoggingIn,
  isLoggedIn,
  token,
  user,
});