import { combineReducers} from "redux";

import * as types from "./types";

// -------------------- MASTERCLASSES -------------------

export function fetchMasterclasses(
  state: types.Masterclass[] = [],
  action: types.MasterclassesActionTypes
) : types.Masterclass[] {
  switch (action.type) {
    case types.FETCH_MASTERCLASSES_STARTED:
      return []
    case types.FETCH_MASTERCLASSES_SUCCESS:
      return [...action.masterclasses]
    case types.FETCH_MASTERCLASSES_FAILURE:
        return []
    default:
      return state;
  }
}

// -------------------- MASTERCLASS COVER -------------------

const selectCover = 
  (state = '', action: types.UploadCoverActionTypes)
    : string => {
  switch (action.type) {
    case types.SELECT_COVER:
      return action.coverUrl
    default:
      return state;
  }
}

const uploadCover = 
 (state = '', action: types.UploadCoverActionTypes)
  : types.Image | string => {
    switch (action.type) {
      case types.UPLOAD_COVER_STARTED:
        return state
      case types.UPLOAD_COVER_FAILURE:
        return action.error
      case types.UPLOAD_COVER_SUCCESS:
        return action.image
      default:
        return state;
    }
  }

const coverIsUploading = 
  (state = false, action: types.UploadCoverActionTypes)
    : boolean => {
  switch (action.type) {
    case types.UPLOAD_COVER_STARTED:
    case types.UPLOAD_COVER_FAILURE:
      return false
    case types.UPLOAD_COVER_SUCCESS:
      return true
    default:
      return state;
  }
}


// ----------------------- UPLOAD VIDEO ---------------------

const selectVideos = 
  (state: types.VIDEO[] = [], action: types.UploadVideoActionTypes)
    : types.VIDEO[] => {
  switch (action.type) {
    case types.SELECT_VIDEOS:
      return action.videos
    default:
      return state;
  }
}

const videoIsUploading = 
  (state = false, action: types.UploadVideoActionTypes)
    : boolean => {
  switch (action.type) {
    case types.UPLOAD_VIDEO_STARTED:
    case types.UPLOAD_VIDEO_FAILURE:
      return false
    case types.UPLOAD_VIDEO_SUCCESS:
        return true
    default:
      return state;
  }
}

// ----------------------- CREATE MASTERCLASS (MC) ------------------------

const createMC = (
  state = '',
  action: types.CreateMCActionTypes
) : string | types.Masterclass => {
  switch (action.type) {
    case types.CREATE_MC_STARTED:
    case types.CREATE_MC_FAILURE:
        return state
    case types.CREATE_MC_SUCCESS:
      localStorage.setItem('Draft MC', 'created')
      return action.masterclass
    default:
      return state;
  }
}

// ----------------------- UPDATE MASTERCLASS (MC) ------------------------

const updateMC = (
  state = '',
  action: types.UpdateMCActionTypes
) : string | types.Masterclass => {
  switch (action.type) {
    case types.UPDATE_MC_STARTED:
    case types.UPDATE_MC_FAILURE:
        return state
    case types.UPDATE_MC_SUCCESS:
      localStorage.setItem('Draft MC', 'created')
      return action.masterclass
    default:
      return state;
  }
}

// ----------------------- GET IMAGE ------------------------

const getImage = (
  state = '',
  action: types.GetImageActionTypes
) : string => {
  switch (action.type) {
    case types.GET_IMAGE_STARTED:
    case types.GET_IMAGE_FAILURE:
        return state
    case types.GET_IMAGE_SUCCESS:
      return action.url
    default:
      return state;
  }
}

// ----------------------- GET Categories ------------------------
const initialState: types.Category[] = []
const categories = (
  state = initialState,
  action: types.GetCategoriesActionTypes
) : types.Category[] => {
  switch (action.type) {
    case types.GET_CATEGORIES_STARTED:
    case types.GET_CATEGORIES_FAILURE:
        return state
    case types.GET_CATEGORIES_SUCCESS:
      return action.categories
    default:
      return state;
  }
}

// ----------------------- EXPORTING ------------------------

export const masterclassReducers = combineReducers({
  fetchMasterclasses,
  selectCover,
  videoIsUploading,
  coverIsUploading,
  selectVideos,
  getImage,
  createMC,
  updateMC,
  categories,
  uploadCover,
});