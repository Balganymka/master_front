import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import ErrorBoundry from './app/components/error_boundry';
import Service from './app/services';
import { ServiceProvider } from './app/components/service_context';
import App from "./app/app";
import configureStore from "./store";

const service = new Service();
const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
      <ErrorBoundry>
        <ServiceProvider value={service}>
          <Router>
            <App/>
          </Router>
        </ServiceProvider>
      </ErrorBoundry>
  </Provider>,
  document.getElementById('root')
);