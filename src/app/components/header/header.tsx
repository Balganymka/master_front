import React, { Component } from 'react';
import Search from '../search';
import './header.scss';
import '../../app.scss';

type State = {
    value: string;
    ownUpdate: boolean;
};

type Props = {
    tag?: string;
    searchVisible?: boolean;
    className?: string;
};

class Header extends Component<Props, State>{
    state = {
        value: '', 
        ownUpdate: false,
    }
    static getDerivedStateFromProps = (nextProps: Props, prevState: State) => {
        if(prevState.ownUpdate){
            return {ownUpdate: false, value: prevState.value};
        } else if(nextProps.tag !== prevState.value){
            return {value: nextProps.tag};
        }
        return null;
    }

    handleSearchChange = (newValue: string) =>{
        this.setState(() => ({
            value: newValue, 
            ownUpdate: true
        }));
    }

    logOut = () => {
        localStorage.clear();
        location.reload();
    }

    render() {
        const {searchVisible=false, className = ''} = this.props;
        const {value} = this.state;
        const classNames = "header ms-flex-inline ms-space-between " + className;
        return ( 
            <div className={classNames}>
                <div className="logo">Master</div>
                <div className="ms-flex-inline ms-align-center">
                    {searchVisible && 
                        <Search value={value} handleChange={this.handleSearchChange}/>
                    }
                    <div className="log-out" onClick={this.logOut}>Log out</div>
                </div>
            </div>
        );
    }
}

export default Header;
