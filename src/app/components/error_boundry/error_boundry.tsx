  
import React, { Component } from 'react';

export default class ErrorBoundry extends Component {

  state = {
    hasError: false
  };

  componentDidCatch() {
    this.setState({
      hasError: true
    });
  }

  render() {

    if (this.state.hasError) {
      return (
        <div className="error-indicator">
            <span className="boom">BOOM!</span>
            <span>
                something has gone wrong
            </span>
        </div>
      )}

    return this.props.children;
  }
}