import React, { Component } from 'react';
import './avatar.scss';
import '../../app.scss';
import DefaultAvatar from '../../images/default-avatar.jpg';

type Props = {
    path?: string,
    width?: number,
    className?: string,
    style?: {},
    avatar?: string,
    username?: string,
}

class Avatar extends Component<Props>{
    render(){
        const {path, className, style={}, avatar, username} = this.props;

        const classNames = `avatar ms-flex-inline ms-align-center ms-justify-center ${className}`;
        return (
            <div className={classNames} style={style}>
                {avatar && <img className="avatar-photo" src={avatar}/>}
                {!avatar && username && <div className="avatar-photo">{username.charAt(0)}</div>}
            </div>
        );
    }
}

export default Avatar;
