import React, { Component } from "react";
import {Router, Route, Redirect, Switch, useLocation} from 'react-router-dom';
import { createBrowserHistory } from 'history'
import { connect} from 'react-redux'

import CreateMasterclass from './containers/create_masterclass';
// import Main from './containers/main';
import Profile from './containers/profile';
import Login from './containers/login';
import Masterclass from './containers/masterclass';
import Register from './containers/register';

import './app.scss';

// const location = useLocation();
const history = createBrowserHistory();

interface AppProps {
    isLoggedIn: boolean
}

class App extends Component<AppProps> {
    render() {
        const {isLoggedIn} = this.props;
        return (
            <Router history={history}>
                <Switch>
                    <AuthRoute path="/login" component={Login} 
                        isLoggedIn={isLoggedIn}/>
                    <AuthRoute path="/register" component={Register} 
                        isLoggedIn={isLoggedIn}/>
                    <UserRoute exact path="/" component={Profile} 
                        isLoggedIn={isLoggedIn}/>
                    <UserRoute path="/new-masterclass" component={CreateMasterclass} 
                        isLoggedIn={isLoggedIn}/>
                    <UserRoute path="/masterclass/:id" component={Masterclass} 
                        isLoggedIn={isLoggedIn}/>
                </Switch>
            </Router>
        );
    }
}

function AuthRoute({component: Component, isLoggedIn, ...rest } : any) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          !isLoggedIn ? (
            <Component {...rest}/>
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

function UserRoute({component: Component, isLoggedIn, ...rest } : any) {
    // console.log('user', ...rest);
    return (
      <Route
        {...rest}
        render={({ location }) =>
          isLoggedIn ? (
            <Component {...rest}/>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

const mapStateToProps = (state: any) => ({
    isLoggedIn: state.auth.isLoggedIn,
})

export default connect(
    mapStateToProps,
    {}
)(App);