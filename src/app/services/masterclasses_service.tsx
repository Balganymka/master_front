import {API_BASE, STD_HEADERS} from './constants';
import qs from 'qs'

const corsProxy = 'https://cors-anywhere.herokuapp.com/'
const getImageUrl = API_BASE + "media/images/";
const MCUrl = API_BASE + "main/masterclass/";
const uploadVideoUrl = API_BASE + "media/upload_video/";
const uploadCoverUrl = API_BASE + "media/images/";
const getCategoriesUrl = API_BASE + "main/category/";

const postData = async (url: string, data: any, token: string) => {
  return await fetch(
    corsProxy + url,
    {
      method: 'POST',
      headers: {
        ...STD_HEADERS,
        'Authorization': `JWT ${token}`,
      },
      body: qs.stringify(data) 
    }
  )};

const getData = async (url: string, token: string) => {
  return await fetch(
    corsProxy + url,
    {
      method: 'GET',
      headers: {
        ...STD_HEADERS,
        'Authorization': `JWT ${token}`,
      }
    }
  )};

const patchData = async (url: string, token: string, data: any) => {
  return await fetch(
    corsProxy + url,
    {
      method: 'PATCH',
      headers: {
        ...STD_HEADERS,
        'Authorization': `JWT ${token}`,
      },
      body: qs.stringify(data)
    }
  );}

const _getImage = async (url: string, token: string) => {
  return await fetch(
    url,
    {
      method: 'GET',
      headers: {
        ...STD_HEADERS,
      }
    }
  )};


const postImage = async (url: string, data: any, token: string) => {
  return await fetch(
    corsProxy + url,
    {
      method: 'POST',
      headers: {
        'Authorization': `JWT ${token}`,
      },
      body: data
    }
  )};

export const uploadVideo = (token: string, data: {file: File}) => (
  postData(uploadVideoUrl, data, token)
)

export const uploadCover = (token: string, data: {image: File}) => {
  const formData = new FormData();
  formData.append('image', data.image);
  return postImage(uploadCoverUrl, formData, token);
}

export const getAllMasterclasses = (token: string) => {
  return getData(MCUrl, token);
}

export const getImage = (token: string, data: string) => {
  return _getImage(`${getImageUrl}/${data}`, token);
}

export const createMC = (token: string) => {
  return postData(MCUrl, null, token)
}

export const updateMC = (token: string, data: any, id: number) => {
  return patchData(`${MCUrl}/${id}/`, token, data)
}

export const getCategories = (token: string) => {
  return getData(getCategoriesUrl, token)
}